
const config = require('./auth.config.json');
const jwt = require('jsonwebtoken');
const Joi = require('joi');
const mongoose = require('mongoose');
const validator = require('validator')
const bcrypt = require('bcrypt')



//simple schema
const UserSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    minlength: 3,
    maxlength: 50
  },
  email: {
    type: String,
    required: true,
    minlength: 5,
    maxlength: 255,
    unique: true
  },
  password: {
    type: String,
    required: true,
    minlength: 3,
    maxlength: 255
  },
  isAdmin: {
    type: Boolean,
    required: true,
    default: false,
  },
  tokens: [{
     token: {
         type: String,
         required: true
     }
 }]
});


UserSchema.pre('save', async function (next) {
  // Hash the password before saving the user model
  const user = this
  if (user.isModified('password')) {
    user.password = await bcrypt.hash(user.password, 8)
  }
  user.isAdmin = config.admin.includes(user.email) ? true : false;
  next()
});

UserSchema.methods.generateAuthToken = async function() {
  // Generate an auth token for the user
  const user = this
  const token = jwt.sign({_id: user._id}, config.eotmjwtkey )
  user.tokens = user.tokens.concat({token})
  await user.save()
  return token
}

//function to validate user
function validateUser(user) {
  const schema = {
    name: Joi.string().min(3).max(50).required(),
    email: Joi.string().min(5).max(255).required().email(),
    password: Joi.string().min(3).max(255).required(),
  };

  return Joi.validate(user, schema);
}

UserSchema.statics.findByCredentials = async (email, password) => {
    // Search for a user by email and password.
    const user = await User.findOne({ email} )
    if (!user) {
      return false;
    }
    const isPasswordMatch = await bcrypt.compare(password, user.password)
    if (!isPasswordMatch) {
      return false;
        // throw new Error({ error: 'Invalid login credentials' })
    }
    return user
}

const User = mongoose.model('User', UserSchema);
exports.User = User;
exports.validate = validateUser;
