const { ArticleTags } = require('./tags.model.js');

const mainSearchTags = [ "knowledge", "eotm" , "paintings", "drawings", "graphics" , "photos" , "infographics", "apps", "books", "lectures", "documentaries", "films" ];

const ArticleTagsService = {
  get: () => {
   return  ArticleTags.find().then((_tags)=>{
      console.log('get all tags');
      return _tags;
    });
  },

  add: (_tags) => {

    const addTag = (_name) => {
      let isMainSearchTag = mainSearchTags.includes(_name) ? true : false;
      new ArticleTags({
        name: _name,
        mainSearchTag: isMainSearchTag,
      }).save().then( _newTag => console.log( 'tag added -> ', _newTag) );
    };

    for (var i = 0; i < _tags.length; i++) {
      let _newTag = _tags[i];
      ArticleTags.countDocuments( { name: _newTag } ).then((_count)=>{
        console.log(_newTag, ' _newTag count ' , _count);
        if(_count < 1){
          console.log('add new tag -> ', _newTag);
          addTag(_newTag);
        }
      });
    };

  },
  search: (_query) => {
    console.log('search tags -> ', _query );
    return ArticleTags.find( { name : { "$regex": _query , "$options": "i" } }, ).then((_tags)=>{
      return _tags;
    });
  }
};

module.exports = { ArticleTagsService };
