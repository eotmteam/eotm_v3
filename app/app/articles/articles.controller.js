const { Articles} = require('./articles.model.js');
const { ArticleTags } = require('./tags/tags.model.js')
const { ArticleTagsService } = require('./tags/tags.service.js')
const auth = require("../users/users.auth.js");
// const authAdmin = require("../users/users.auth.js");

module.exports = function(app) {

  require('./tags/tags.controller.js')(app, ArticleTagsService);

  app.post('/article/delete', auth , (req, res) => {
    let aHref = req.body.href;
    Articles.deleteOne( { href: aHref }).then(
      suc => {
        res.send(suc);
      },
      err => {
        res.send(err);
      }
    );
  });

  app.post('/articles/add', auth , (req, res) => {
    var newArticles = req.body;
    var addedArticles = [];

    console.log('add multiple articles');

    newArticles.forEach( ( item, index ) => {
      newArticleCreate(item).save().then(
        addedArticle => {
          addedArticles.push(addedArticle);
          console.log( 'added article -> ', addedArticle.href );
          if( index == newArticles.length - 1 ) {
            console.log( 'number of added articleS -> ', addedArticles.length );
            res.send(addedArticles);
          }
        }, err => { res.send(err); });
      })

    });

    const newArticleCreate = (_data) => {
      console.log('create article data');
      return new Articles({
        href: _data.href,
        category: _data.category,
        subcategory: _data.subcategory,
        tags: _data.tags,
        content: _data.content,
        thumb: _data.thumb,
        status: _data.status,
        dateCreated: _data.dateCreated,
        // gallery: _data.gallery,
      });
    }

    const timeOfCreation = () => {
      var date = new Date().getTime();
      return date;
    };

    const galleryPicturesArrayCreate = (_galleryData) => {
      if(!_galleryData.size || !_galleryData.format ){
        return [];
      }else{
        let galleryContent = [];
        for (var i = 1; i < Number(_galleryData.size) + 1; i++) {
          let num = i < 10 ? '0' + i : i;
          galleryContent.push([ num + _galleryData.format , num + 's' + _galleryData.format ]);
        }
        return galleryContent;
      }
    }

    const thumbDataCreate = (_thumb, _content, _href) => {
      let thumbData = { title : _content.title , description : _content.description, picture : { path:_href + _thumb.picture.format  , format: _thumb.picture.format }  };
      return thumbData;
    };

    const updateArticleData = (_reqBody) => {
      if(_reqBody.content && _reqBody.content.gallery){
        _reqBody.content.gallery.pictures = galleryPicturesArrayCreate(_reqBody.content.gallery);
      }
      _reqBody.thumb = thumbDataCreate(_reqBody.thumb , _reqBody.content, _reqBody.href);
      return _reqBody;
    }

    app.post('/article/add', auth, (req, res) => {

      req.body = updateArticleData(req.body)
      req.body.dateCreated = timeOfCreation();

      newArticleCreate(req.body).save().then(
        doc => {
          console.log('add article -> ', doc.href);
          ArticleTagsService.add(doc.tags);
          res.send(doc);
        }, err => {
          res.send(err);
        });
      });

      app.post('/article/update/:href', auth , (req,res ) => {

        req.body = updateArticleData(req.body)

        Articles.updateOne({href: req.params.href }, req.body ).then((_data) => {
          console.log(_data , ' -> updated -> ',  req.body.href);
          ArticleTagsService.add(req.body.tags);
          res.send(_data);
        }, (err) => {
          res.send(err);
        })


      });

      const statusFilterCreate = (_stateQuery) => {

        let articleStatus = _stateQuery ? _stateQuery : 'live'; // -> live / ready / all

        if(articleStatus == 'ready'){
          return { "status.ready" : true};
        }else if (articleStatus == 'all') {
          return {};
        }

        return { "status.live" : true };

      }

      app.get('/article/:href', (req,res ) => {

        console.log('get Article');

        Articles.findOne({href: req.params.href}).then( ( _article ) => {

          const tagFilter = _article.tags.includes("eotm") ? "eotm" : "knowledge";
          const articleDateCreated = new Date( _article.dateCreated );

          const relatedArticlePrevSearch = {
            $and: [
              { tags: tagFilter } ,
              { "status.live" : true } ,
              { dateCreated: {
                $lt: articleDateCreated,
              }},
            ],
          };
          const relatedArticleNextSearch = {
            $and: [
              { tags: tagFilter } ,
              { "status.live" : true } ,
              { dateCreated: {
                $gt: articleDateCreated ,
              }},
            ],
          };

          let relatedArticles = {   next: false,   prev: false }

          Articles.find( relatedArticleNextSearch )
          .sort({ dateCreated : 1 })
          .limit(1)
          .then( relatedNext => {
            if(relatedNext[0]){
              relatedArticles.next = {};
              relatedArticles.next.title = relatedNext[0].content.title;
              relatedArticles.next.href = relatedNext[0].href;
            }

            Articles.find( relatedArticlePrevSearch )
            .sort({ dateCreated : -1 })
            .limit(1)
            .then( relatedPrev => {
              if(relatedPrev[0]){
                relatedArticles.prev = {};
                relatedArticles.prev.title = relatedPrev[0].content.title;
                relatedArticles.prev.href = relatedPrev[0].href;
              }

              _article._doc.relatedArticles = relatedArticles;
              res.send( _article );

            })
          })


        }, (err) => {
          res.send(err);
        })
      });

      app.get('/articles/get/:pageSize/:pageNumber', (req,res ) => {

        let filterArticleStatus = statusFilterCreate(req.query.articlestatus);

        Articles.find(filterArticleStatus)
        .skip(parseInt(req.params.pageSize * (req.params.pageNumber - 1)) )
        .sort({ dateCreated : -1 })
        .limit(parseInt(req.params.pageSize))
        .then( ( _articles ) => {

          const articles = _articles;

          Articles.find( filterArticleStatus )
          .skip(parseInt(req.params.pageSize * (req.params.pageNumber)) )
          .sort({ dateCreated : -1 })
          .limit(parseInt(req.params.pageSize))
          .countDocuments().then((count) => {
            const nextPageAvailable = count > 0 ? true : false;

            res.send( {articles: articles , nextPageAvailable: nextPageAvailable} );
          });

        }, (err) => {
          console.log('error -> ', err);
          res.send(err);
        })

      });


      app.get('/articles/search/:searchword', (req,res ) => {

        let filterArticleStatus = statusFilterCreate(req.query.articlestatus);

        let searchWord = req.params.searchword == 'null'  || req.params.searchword == 'undefined' || req.params.searchword.length == 0 ? false : req.params.searchword ;
        let searchTagsArray = req.query.tags.split(',')[0] == 'undefined' ? false : req.query.tags.split(',') ;
        let currentPage = req.query.page;
        let pageSize = req.query.pagesize;

        console.log('search word = ' , searchWord, ' / search tags = ',  searchTagsArray);

        let  searchOptions = {
          $and: [filterArticleStatus],
          $or: [],
        };

        let searchOptionsTags = { tags: { $all: searchTagsArray } };
        let searchOptionsWord = {
          // $and: [
          $or: [
            { href : { "$regex": searchWord , "$options": "i" } },
            { "content.title" : { "$regex": searchWord , "$options": "i" } },
            { "content.description" : { "$regex": searchWord , "$options": "i" } },
            { "content.gallery.description" : { "$regex": searchWord , "$options": "i" } },
            // add search in knowledge and gallery content
          ]
        };

        if(searchWord){
          searchOptions.$or.push(searchOptionsWord);
          // searchOptions.$and.push(searchOptionsWord);
        }
        if (searchTagsArray) {
          searchOptions.$or.push(searchOptionsTags);
          // searchOptions.$and.push(searchOptionsTags);
        }

        Articles.find(searchOptions)
        .skip(parseInt(pageSize * (currentPage - 1)) )
        .sort({ dateCreated : -1 })
        .limit(parseInt(pageSize))
        .then((articles) => {

          Articles.find( searchOptions )
          .skip(parseInt(pageSize * (currentPage)) )
          .sort({ dateCreated : -1 })
          .limit(parseInt(pageSize))
          .count().then((count) => {
            const nextPageAvailable = count > 0 ? true : false;
            console.log( 'articles num -> ',  articles.length );
            res.send( {articles: articles , nextPageAvailable: nextPageAvailable} );
          });

          // res.send(articles)
        }, (err) => { res.send(err) })
      });

    };
