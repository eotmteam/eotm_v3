const mongoose = require('mongoose');

const ArticleModelOpt = {
  href : {
    type: String,
    required: true,
    minlength: 3,
    maxlength: 50,
    trim: true,
    unique: true
  },
  shopLink:{
    type: String,
  },
  tags: {
    type: Array,
  },
  thumb: {
    title: {
      type: String,
    },
    description:{
      type: String,
    },
    picture : {
      path: {
        type: String,
        // required: true,
      },
      format: {
        type: String,
        // required: true,
      }
    }
  },
  content: {
    title: {
      type: String,
      maxlength: 50,
      required: true,
    },
    description: {
      type: String,
      maxlength: 250,
    },
    knowledge: [{
      description : {
        type: String,
      },
      videoUrl : {
        type: String,
      },
      picture: {
        type: String,
      }
    }
    ],
    gallery : {
      description: {
        type: String,
      },
      mainPicture : {
        type: Boolean,
      },
      size : {
        type: Number,
      },
      format : {
        type: String,
      },
      pictures: {
        type: Array,
      }
    },
    sources : {
      type: Array,
    }
  },
  dateCreated : {
    type: Date,
  },
  status: {
    ready : {
      type: Boolean,
      default: false
    },
    live : {
      type: Boolean,
      default: false
    }
  }

};

var Articles = mongoose.model('Article' , ArticleModelOpt );

// var ArticleReady = mongoose.model('ReadyArticle' , ArticleModelOpt );
// var ArticleAll = mongoose.model('AllArticle' , ArticleModelOpt );
// var ArticleLive = mongoose.model('LiveArticle' , ArticleModelOpt );

module.exports = { Articles };
