# README #


### What is this repository for? ###

* EOTM.INFO backend
* Version 1.0

### How do I get set up? ###

* Node.js
* nvm use node 8 or 12
* npm install to install all dependencies


### run mongo DB ###

* sudo service mongod start
* mongo
* npm run dev

### heroku deploy ###

* git push heroku master
