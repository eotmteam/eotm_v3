# README #

EOTM V3.0 - contains Front-end VueJS and Back-end in ExpressJS

### run Front ###

* nvm use 14
* npm run dev

### run Back-end: Database ###

* sudo service mongod start
* mongo

### run Back-end: NodeJS app ###
* nvm use 12
* npm run dev
