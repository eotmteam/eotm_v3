var path = require('path')
var webpack = require('webpack')
// var autoprefixer = require('autoprefixer');
// var precss = require('precss');

const VueLoaderPlugin = require('vue-loader/lib/plugin');
const VersioningPlugin = require('versioning-webpack-plugin');
const WebpackMd5Hash = require('webpack-md5-hash');
const HtmlWebpackPlugin = require('html-webpack-plugin');


module.exports = {
  // entry: './src/app.js',
  entry: {
    eotm: ['./src/app.js' ],
  },
  output: {
    filename: process.env.NODE_ENV === 'production' ?  "[name].[hash].js" : "[name].js",
    path: path.resolve(__dirname, 'dist'),
    publicPath: '/',
    // publicPath: path.resolve(__dirname, 'dist')
  },
  devServer: {
    contentBase:  path.join(__dirname, 'src'),
    compress: false,
    port: 4200,
    hot: true,
    historyApiFallback: true,
  },
  optimization: {
    minimize:  process.env.NODE_ENV === 'production' ? true : false
  },
  module: {
    rules: [
      {
        test: /\.vue$/,
        loader: 'vue-loader',
        options: {
          loaders: {
            'scss': 'vue-style-loader!css-loader!sass-loader',
            'sass': 'vue-style-loader!css-loader!sass-loader?indentedSyntax'
          },
          target: 'node-webkit',
          // other vue-loader options go here
        }
      },
      {
        test: /\.m?js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env']
          }
        }
      },
      // {
      //   test: /\.js$/,
      //   loader: 'babel-loader',
      //   exclude: /node_modules/
      // },
      {
        test: /\.scss$/,
        use: [
          'vue-style-loader',
          "css-loader",
          // "sass-loader",
          {
            loader: 'sass-loader',
            options: {
              data: `
              @import "./src/assets/scss/_config.scss";

              `
            }
          },
        ]
      },
      {
        test: /\.(png|jpg|gif|svg)$/,
        loader: 'file-loader',
        options: {
          // name: '[name].[ext]?[hash]'
          name: '/[name].[ext]'
        }
      },
      {
        test: /\.(woff2?|ttf|otf|eot|svg|woff|woff2)$/,
        exclude: /node_modules/,
        loader: 'file-loader',
        options: {
          name: '/[name].[ext]'
        }
      },
      {
        test: /\.(glsl|vert|frag)$/,
        loader: 'threejs-glsl-loader',
        // Default values (can be omitted)
        options: {
          chunksPath: '../ShaderChunk', // if chunk fails to load with provided path (relative), the loader will retry with this one before giving up
          chunksExt: 'glsl', // Chunks extension, used when #import statement omits extension
        }
      }
    ],
  },
  // devtool: 'source-map',
  // devServer: {
  //   // contentBase: 'public'
  // },
  //   resolve: {
  //     alias: {
  //       'vue$': 'vue/dist/vue.esm.js'
  //     },
  //     extensions: ['*', '.js', '.vue', '.json']
  // },
  plugins: [
    new VueLoaderPlugin(),
    new VersioningPlugin({
      cleanup: true,                      // should it remove old files?
      basePath: '../',                     // manifest.json base path
      manifestFilename: 'versioning.json'   // name of the manifest file
    }),
    new WebpackMd5Hash(),
    new HtmlWebpackPlugin({
      chunks: ["eotm"],
      template: './src/index.html',
      filename: "index.html"
    }),
  ]
};
