import Vue from 'vue'
import Router from 'vue-router'
import ThumbsContainer from './app/thumbs/ThumbsContainer.vue'
import Article from './app/article/Article.vue'
import About from './app/About.vue'
import Shop from './app/shop/Shop.vue'
import CommingSoon from './app/shop/commingSoon.vue'

import AdminArticle from './app/admin/AdminArticle.vue'
import AdminThumbs from './app/admin/AdminThumbs.vue'
import AdminLogin from './app/admin/AdminLogin.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',

  routes: [{
      path: '/',
      name: 'Home',
      component: ThumbsContainer
    },
    {
      path: '/search/:searchword',
      name: 'Search',
      props: true,
      component: ThumbsContainer
    },
    {
      path: '/article/:href',
      name: 'Article',
      props: true,
      component: Article
    },
    {
      path: '/about',
      name: 'About',
      component: About
    },
    {
      path: '/shop',
      name: 'Shop',
      component: CommingSoon
      // component: Shop
    },
    {
      path: '/admin/article/:href',
      name: 'AdminArticle',
      props: true,
      component: AdminArticle
    },
    {
      path: '/admin/thumbs',
      name: 'AdminThumbs',
      props: true,
      component: AdminThumbs
    },
    {
      path: '/admin',
      name: 'AdminLogin',
      props: true,
      component: AdminLogin
    },
  ],
  scrollBehavior(to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition
    } else {
      return {
        x: 0,
        y: 0
      }
    }
  }
})
