import './config/config.js';
import Vue from 'vue';
import App from './App.vue';
import router from './routes.js';

import {DisplayService} from './app/utilities/displayService.js'
// import {CanvasService} from  './app/utilities/canvas/canvasService.js'
// import VueParticles from 'vue-particles';
import VueResource from 'vue-resource';

// --------------------------------------------------
// Common GLOBAL Directives
// --------------------------------------------------
import {imgLoader} from './app/utilities/imgLoader';

Vue.directive('imgloader', imgLoader );

Vue.config.performance = true;

Vue.use(VueResource)

import store from './app/appStore';

DisplayService.init();

let EOTMAPP =  new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
});
