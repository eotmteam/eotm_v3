import axios from 'axios';
if(window._eotmConfig.mode === 'development'){
  axios.defaults.withCredentials = false;
}

 class DesignClapClass {

  constructor(){
    this.clapsURL = window._eotmConfig[window._eotmConfig.mode].apiURL + '/design-claps/';
    // this.thumbsPage = 15;
  }

  getClaps(){
    return axios.get( this.clapsURL  ).then(  (response) =>  {
      return response.data;
    })
    .catch( (error) =>  {
      console.log(error);
      return error;
    });
  }

  clap(_designId){
    return axios.post( this.clapsURL + _designId  ).then(  (response) =>  {
      return response.data;
    })
    .catch( (error) =>  {
      console.log(error);
      return error;
    });

  }

}

const DesignClapService = new DesignClapClass();

export { DesignClapService }
