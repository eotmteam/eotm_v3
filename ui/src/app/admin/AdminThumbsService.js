import axios from 'axios';
if(window._eotmConfig.mode === 'development'){
  axios.defaults.withCredentials = false;
}

class AdminThumbsServiceClass {

  constructor(){
    this.getThumbsURL = window._eotmConfig[window._eotmConfig.mode].apiURL + '/articles/get/';
    this.searchThumbsURL = window._eotmConfig[window._eotmConfig.mode].apiURL + '/articles/search/';
    this.thumbsPage = 150;
    // this.articleStatusQueryCreate = (_status) => {
    //
    // }
  }
  getThumbsPage(_page , _status, _andNotPub){
    let statusQuery = '?articlestatus=' + _status;
    return axios.get( this.getThumbsURL + this.thumbsPage + '/' + _page + statusQuery ).then(  (response) =>  { // add query  + ?articlestatus=all|ready|live , if query empty == life
      return response.data;
    })
    .catch( (error) =>  {
      console.log(error);
      return error;
    });
  }
  searchThumbsPage( _searchWord, _tags, _page , _status, _andNotPub){
    let statusQuery = '&articlestatus=' + _status;
    return axios.get( this.searchThumbsURL + _searchWord + '?tags=' + _tags + '&page=' + _page + '&pagesize=' + this.thumbsPage + statusQuery ).then(  (response) =>  {  // add query  + '?articlestatus=all|ready|live , if query empty == life
      return response.data;
    })
    .catch( (error) =>  {
      console.log(error);
      return error;
    });
  }

}
const AdminThumbsService = new AdminThumbsServiceClass();
export { AdminThumbsService }
