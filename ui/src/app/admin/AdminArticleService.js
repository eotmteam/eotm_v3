import axios from 'axios';
if(window._eotmConfig.mode === 'development'){
  axios.defaults.withCredentials = false;
}

const urlBase = window._eotmConfig[window._eotmConfig.mode].apiURL;

 class AdminArticleServiceClass {

  constructor(){
    this.updateURL = urlBase + '/article/update/';
    this.deleteURL = urlBase + '/article/delete/';
    this.addURL = urlBase + '/article/add/';
    // this.thumbsPage = 15;
  }

  update(_articleData){
    const token =  localStorage.getItem("eotmAdminUserToken");
    return axios.post( this.updateURL + _articleData.href ,  _articleData , { headers: { "Authorization" : 'Bearer ' + token }} ).then(  (response) =>  {
      return response.data;
    })
    .catch( (error) =>  {
      console.log(error);
      return error;
    });

  }

  delete(_articleHref ){
    const token =  localStorage.getItem("eotmAdminUserToken");
    return axios.post( this.deleteURL , { "href":  _articleHref } , { headers: { "Authorization" : 'Bearer ' + token }} ).then(  (response) =>  {
      return response.data;
    })
    .catch( (error) =>  {
      console.log(error);
      return error;
    });
  }

  add(_articleHref, _articleTitle){
    const token =  localStorage.getItem("eotmAdminUserToken");
    let articleBody = {
      href: _articleHref,
      thumb: {
        picture : {
          format: '.png', //set default .png format
        }
      },
      content: {
        title: _articleTitle,
      }
    }
    return axios.post( this.addURL , articleBody , { headers: { "Authorization" : 'Bearer ' + token }} ).then(  (response) =>  {
      return response.data;
    })
    .catch( (error) =>  {
      console.log(error);
      return error;
    });

  }


}

const AdminArticleService = new AdminArticleServiceClass();

export { AdminArticleService }
