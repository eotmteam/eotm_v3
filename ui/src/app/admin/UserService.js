import axios from 'axios';
if(window._eotmConfig.mode === 'development'){
  axios.defaults.withCredentials = false;
}

 class UserServiceClass {

  constructor(){
    this.getCurrentURL = window._eotmConfig[window._eotmConfig.mode].apiURL + '/user/current';
    this.loginURL = window._eotmConfig[window._eotmConfig.mode].apiURL + '/user/login';
    this.logoutURL = window._eotmConfig[window._eotmConfig.mode].apiURL + '/user/current/logout';
  }

  getCurrent(){
    const token =  localStorage.getItem("eotmAdminUserToken");
    return axios.get( this.getCurrentURL, { headers: { "Authorization" : 'Bearer ' + token }} ).then(  (response) =>  {
      return response.data;
    })
    .catch( (error) =>  {
      console.log(error);
      return false;
    });
  }

  login( _email, _password ){

    let loginData = { email: _email , password: _password };

    return axios.post( this.loginURL, loginData ).then(  (response) =>  {
      localStorage.setItem("eotmAdminUserToken", response.data.token);
      return response.data;
    })
    .catch( (error) =>  {
      console.log(error);
      return false;
    });
  }
  logout(){
    const token =  localStorage.getItem("eotmAdminUserToken");
    localStorage.removeItem("eotmAdminUserToken");
    return axios.post( this.logoutURL, {}, { headers: { "Authorization" : 'Bearer ' + token }} ).then(  (response) =>  {
      return response.data;
    })
    .catch( (error) =>  {
      console.log(error);
      return false;
    })

  }

}

const UserService = new UserServiceClass();

export { UserService }
